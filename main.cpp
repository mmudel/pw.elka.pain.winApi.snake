#include <Windows.h>
#include <deque>
#include <vector>
#include <time.h>
#include <iostream>

#define SNAKE_WIDTH 10
#define ID_TIMER 1
#define GAME_SPEED 50
#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 1000

using namespace std;

LONG WINAPI WndProc(HWND, UINT, WPARAM, LPARAM);
HWND InitializeWindow(HINSTANCE, HINSTANCE, LPSTR, int);
void Draw(HDC*);

COLORREF snakeColor = RGB(0, 0, 100);
COLORREF headColor = RGB(100, 0, 0);
COLORREF backgroundColor = RGB(255, 255, 255);
COLORREF appleColor = RGB(0, 100, 0);
HWND myWindow;
HWND secondWindow;

int startMessage = RegisterWindowMessage("START");
int startMessageReply = RegisterWindowMessage("START_REPLY");
int snakeChangeWindowMessage = RegisterWindowMessage("SNAKE_CHANGE_WINDOW");

float GetPixelsPerMM_LOMETRIC()
{
	HDC screen = GetDC(NULL);
	int hSize = GetDeviceCaps(screen, HORZSIZE);
	int hRes = GetDeviceCaps(screen, HORZRES);
	float PixelsPerMM = (float)hRes / hSize;
	return PixelsPerMM / 10;
}

enum Direction
{
	NORTH,
	EAST,
	SOUTH,
	WEST
};

struct Point
{
	int x;
	int y;
	Point(int x, int y) : x(x), y(y) {};
	Point() {};
};

struct MessageInfo
{
	int length;
	Direction direction;
	MessageInfo() {}

	MessageInfo(int toParse)
	{
		direction = (Direction)(toParse & 0x00000003);
		length = ((toParse >> 2) & 0x000000FF);
	}

	int ParseToInt()
	{
		return (int)direction |
			(length << 2);
	}
};

class Apple
{
public:
	Point point;
	Apple()
	{
		point.x = 150 + rand() % (WINDOW_WIDTH - 300);
		point.x = point.x + point.x % 4;
		point.y = 150 + rand() % (WINDOW_HEIGHT - 300);
		point.y = point.y + point.y % 4;
	}
	void Draw(HDC* hdc)
	{
		HPEN hPen;
		hPen = CreatePen(PS_SOLID, SNAKE_WIDTH, appleColor);
		SelectObject(*hdc, hPen);
		Rectangle(*hdc, point.x + SNAKE_WIDTH / 2, -(point.y + SNAKE_WIDTH / 2), point.x + SNAKE_WIDTH + SNAKE_WIDTH / 2, -(point.y + SNAKE_WIDTH + SNAKE_WIDTH / 2));
		DeleteObject(hPen);
	}
	void Delete(HDC* hdc)
	{
		HPEN hPen;
		hPen = CreatePen(PS_SOLID, SNAKE_WIDTH, backgroundColor);
		SelectObject(*hdc, hPen);
		Rectangle(*hdc, point.x + SNAKE_WIDTH / 2, -(point.y + SNAKE_WIDTH / 2), point.x + SNAKE_WIDTH + SNAKE_WIDTH / 2, -(point.y + SNAKE_WIDTH + SNAKE_WIDTH / 2));
		DeleteObject(hPen);
	}
};

vector<Apple*> apples;

class Snake
{
public:
	deque<Point> snakeBody;
	Direction direction;
	int length;
	Point head;
	bool windowChanged = false;
	bool eaten = false;

	Snake(deque<Point> snakeBody, Direction direction, int length) : snakeBody(snakeBody), direction(direction), length(length) { head = snakeBody.front(); }
	Snake() {};

	void Draw(HDC* hdc)
	{
		for (Point point : snakeBody)
		{
			HPEN hPen;
			if(point.x == head.x && point.y == head.y)
				hPen = CreatePen(PS_SOLID, SNAKE_WIDTH, headColor);
			else
				hPen = CreatePen(PS_SOLID, SNAKE_WIDTH, snakeColor);
			SelectObject(*hdc, hPen);
			if(point.x >= 0 && point.x < WINDOW_WIDTH - 20 && point.y >= 0 && point.y < WINDOW_HEIGHT - 20)
				Rectangle(*hdc, point.x + SNAKE_WIDTH/2, -(point.y + SNAKE_WIDTH / 2), point.x + SNAKE_WIDTH + SNAKE_WIDTH / 2, -(point.y + SNAKE_WIDTH + SNAKE_WIDTH / 2));
			DeleteObject(hPen);
		}
	}

	int Move(HDC *hdc)
	{
		int newY, newX;
		Point oldPoint = snakeBody.back();
		if (eaten == false)
		{
			HPEN hPen = CreatePen(PS_SOLID, SNAKE_WIDTH, backgroundColor);
			SelectObject(*hdc, hPen);
			Rectangle(*hdc, oldPoint.x + SNAKE_WIDTH / 2, -(oldPoint.y + SNAKE_WIDTH / 2), oldPoint.x + SNAKE_WIDTH + SNAKE_WIDTH / 2, -(oldPoint.y + SNAKE_WIDTH + SNAKE_WIDTH / 2));
			DeleteObject(hPen);
			snakeBody.pop_back();
		}
		else
			eaten = false;
		switch (direction)
		{
		case NORTH:
			newY = -SNAKE_WIDTH-SNAKE_WIDTH/2;
			newX = 0;
			break;
		case EAST:
			newY = 0;
			newX = SNAKE_WIDTH+SNAKE_WIDTH/2;
			break;
		case SOUTH:
			newY = SNAKE_WIDTH+SNAKE_WIDTH/2;
			newX = 0;
			break;
		case WEST:
			newY = 0;
			newX = -SNAKE_WIDTH-SNAKE_WIDTH/2;
		}
		Point newHead(head.x + newX, head.y + newY);
		snakeBody.push_front(newHead);
		head = newHead;
		for (int i = 0; i < apples.size(); i++)
		{
			if (apples[i]->point.x < newHead.x + 20 && apples[i]->point.x > newHead.x - 20 && apples[i]->point.y < newHead.y + 20 && apples[i]->point.y > newHead.y - 20)
			{
				apples[i]->Delete(hdc);
				delete(apples[i]);
				apples.erase(apples.begin() + i);
				Apple *apple1 = new Apple();
				apples.push_back(apple1);
				this->Eat();
			}
		}
		if ((newHead.x < 0 || newHead.x > WINDOW_WIDTH - 20 || newHead.y < 0 || newHead.y > WINDOW_HEIGHT-20) && windowChanged == false)
		{
			windowChanged = true;
			MessageInfo msg;
			int secondCoord;
			msg.direction = direction;
			msg.length = length;
			int newMessage = msg.ParseToInt();
			MessageInfo msg2(newMessage);
			if (direction == NORTH || direction == SOUTH)
				secondCoord = head.x;
			else
				secondCoord = head.y;
			BringWindowToTop(secondWindow);
			PostMessage(secondWindow, snakeChangeWindowMessage, newMessage, secondCoord);
		}
		if ((snakeBody.back().x < 0 || snakeBody.back().x >= WINDOW_WIDTH - 20 || snakeBody.back().y < 0 || snakeBody.back().y >= WINDOW_HEIGHT - 20) && windowChanged)
			return 1;
		return 0;
	}

	void Eat()
	{
		eaten = true;
		length++;
	}

	void changeDirection(WPARAM newDirection)
	{
		switch (newDirection)
		{
		case VK_UP:
			if (direction != SOUTH)
				direction = NORTH;
			break;
		case VK_RIGHT:
			if (direction != WEST)
				direction = EAST;
			break;
		case VK_DOWN:
			if (direction != NORTH)
				direction = SOUTH;
			break;
		case VK_LEFT:
			if (direction != EAST)
				direction = WEST;
			break;
		}
	}
};

Snake *playerSnake;
vector<Snake*> snakes;

void createPlayerSnake()
{
	deque<Point> points;
	points.push_back(Point(2, 56));
	points.push_back(Point(2, 50));
	points.push_back(Point(2, 44));
	points.push_back(Point(2, 38));
	points.push_back(Point(2, 32));
	points.push_back(Point(2, 26));
	points.push_back(Point(2, 20));
	points.push_back(Point(2, 14));
	points.push_back(Point(2, 8));
	points.push_back(Point(2, 2));

	playerSnake = new Snake(points, EAST, 10);

	snakes.push_back(playerSnake);

}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	MSG msg;
	Apple *apple = new Apple();
	apples.push_back(apple);
	srand(time(NULL));
	myWindow = InitializeWindow(hInstance, hPrevInstance, lpszCmdLine, nCmdShow);
	int a = sizeof(Point);
	int b = sizeof(Direction);
	PostMessage(HWND_BROADCAST, startMessage, (int)myWindow, 0);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

HWND InitializeWindow(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{
	static char szAppName[] = "Gniazdo �mij";
	WNDCLASS wc;
	HWND hwnd;
	wc.style = 0;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = CreateSolidBrush(backgroundColor);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = szAppName;
	RegisterClass(&wc);

	hwnd = CreateWindow(szAppName,
		szAppName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		WINDOW_WIDTH * GetPixelsPerMM_LOMETRIC(), WINDOW_HEIGHT * GetPixelsPerMM_LOMETRIC(),
		HWND_DESKTOP,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
	SetTimer(hwnd, ID_TIMER, GAME_SPEED, NULL);

	return hwnd;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	RECT rect;
	if (message == startMessage && (WPARAM)hwnd != wParam)
	{
		secondWindow = (HWND)wParam;
		PostMessage(secondWindow, startMessageReply, (WPARAM)hwnd, NULL);
		return 0;
	}
	if (message == startMessageReply)
	{
		secondWindow = (HWND)wParam;
		createPlayerSnake();
		return 0;
	}
	if (message == snakeChangeWindowMessage)
	{
		int xModifier, yModifier, newX, newY, x, y;
		MessageInfo msg(wParam);
		deque<Point> snakeBody;
		if (msg.direction == NORTH)
		{
			x = lParam;
			y = WINDOW_HEIGHT - SNAKE_WIDTH;
		}
		else if (msg.direction == SOUTH)
		{
			x = lParam;
			y = 0;
		}
		else if (msg.direction == EAST)
		{
			y = lParam;
			x = 0;
		}
		else
		{
			y = lParam;
			x = WINDOW_WIDTH - SNAKE_WIDTH;
		}
		snakeBody.push_back(Point(x, y));
		switch(msg.direction)
		{
		case NORTH:
			xModifier = 0;
			yModifier = SNAKE_WIDTH;
			break;
		case EAST:
			xModifier = -SNAKE_WIDTH;
			yModifier = 0;
			break;
		case SOUTH:
			xModifier = 0;
			yModifier = -SNAKE_WIDTH;
			break;
		case WEST:
			xModifier = SNAKE_WIDTH;
			yModifier = 0;
			break;
		}
		newX = x;
		newY = y;
		for (int i = 1; i < msg.length; i++)
		{
			newX = newX + xModifier;
			newY = newY + yModifier;
			Point point(newX, newY);
			snakeBody.push_back(point);
		}

		playerSnake = new Snake(snakeBody, msg.direction, msg.length);
		snakes.push_back(playerSnake);
		return 0;
	}
	switch (message)
	{
	case WM_TIMER:
		hdc = GetDC(hwnd);
		SetMapMode(hdc, MM_LOMETRIC);
		for (int i = 0; i < snakes.size(); i++)
		{
			if (snakes[i]->Move(&hdc))
			{
				delete(snakes[i]);
				snakes.erase(snakes.begin() + i);
			}
			else
				snakes[i]->Draw(&hdc);
		}
		for (Apple *apple : apples)
			apple->Draw(&hdc);
		ReleaseDC(hwnd, hdc);
		break;
	case WM_PAINT:
	case WM_CREATE:
		hdc = BeginPaint(hwnd, &ps);
		SetMapMode(hdc, MM_LOMETRIC);
		for (Snake *snake : snakes)
			snake->Draw(&hdc);
		for (Apple *apple : apples)
			apple->Draw(&hdc);
		EndPaint(hwnd, &ps);
		return 0;
	case WM_KEYDOWN:
		playerSnake->changeDirection(wParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
}